//
//  Flicker_Photo_ViewerTests.swift
//  Flicker Photo ViewerTests
//
//  Created by Ramon Vasconcelos on 07/09/2017.
//  Copyright © 2017 Ramon Vasconcelos. All rights reserved.
//

import XCTest
import Alamofire
import SwiftyJSON
import CoreData

@testable import Flicker_Photo_Viewer

class Flicker_Photo_ViewerTests: XCTestCase {

    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }

    func deleteOldDataFromCoreData() {
        guard let objectsToDelete = CoreDataManager().fetchPhotosFromCoreData().1 else {
            return
        }
        CoreDataManager().deletePhotosFromCoreData(objectsToDelete)
    }

    func checkCoreDataSave(_ receivedPhotosArray: [Photo]) {
        let coreDataObjects = CoreDataManager().savePhotos(receivedPhotosArray)
        checkCoreDataPersistence()
        checkDeleteFromCoreData(coreDataObjects)
        let result = coreDataObjects.count > 0
        XCTAssertTrue(result)
    }

    func checkCoreDataPersistence() {
        let result = CoreDataManager().entityIsEmpty("PhotoEntity")
        XCTAssertFalse(result)
    }

    func checkDeleteFromCoreData(_ coreDataPhotoObjects: [NSManagedObject]) {
        CoreDataManager().deletePhotosFromCoreData(coreDataPhotoObjects)
        let result = CoreDataManager().entityIsEmpty("PhotoEntity")
        XCTAssertTrue(result)
    }

    func testFlickerGetPhotos() {
        guard let url = URL(string: baseUrl) else {
            XCTFail("Could not load url")
            return
        }

        let ex = expectation(description: "Expecting a JSON data not nil")

        let parameters = ["method": FlickerMethods.getRecentPhotos,
                          "api_key": apiKey,
                          "format": "json",
                          "nojsoncallback": "1"]
        Alamofire.request(url, parameters: parameters).responseJSON { (response) in
            switch response.result {
            case .success(let value):
                self.deleteOldDataFromCoreData()
                let responseJSON = JSON(value)
                guard let photos = responseJSON["photos"]["photo"].array else {
                    XCTFail("Could not load photos")
                    return
                }
                var photosArray = [Photo]()
                for json in photos {
                    let photo = Photo(withJson: json)
                    print(photo.title ?? "")
                    photosArray.append(photo)
                }
                self.checkCoreDataSave(photosArray)
                ex.fulfill()
                XCTAssert(photosArray.count > 0)
            case .failure(let error):
                XCTFail("error: \(error.localizedDescription)")
            }
        }

        waitForExpectations(timeout: 10) { (error) in
            if let error = error {
                XCTFail("error: \(error)")
            }
        }

    }

}
