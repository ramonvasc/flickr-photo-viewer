//
//  Constants.swift
//  Flicker Photo Viewer
//
//  Created by Ramon Vasconcelos on 07/09/2017.
//  Copyright © 2017 Ramon Vasconcelos. All rights reserved.
//

import UIKit

    let baseUrl = "https://api.flickr.com/services/rest/"
    let apiKey = "f74bc611c54a58fe2a806bfff748df97"

struct FlickerMethods {
    static let getRecentPhotos = "flickr.photos.getRecent"
}

enum FlickerPhotoSize: String {
    case small = "s"
    case medium = "m"
}
