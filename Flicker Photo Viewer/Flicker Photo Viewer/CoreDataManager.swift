//
//  CoreDataManager.swift
//  Flicker Photo Viewer
//
//  Created by Ramon Vasconcelos on 07/09/2017.
//  Copyright © 2017 Ramon Vasconcelos. All rights reserved.
//

import UIKit
import CoreData
import SwiftyJSON

class CoreDataManager {

    func entityIsEmpty(_ entityName: String) -> Bool
    {
        let managedContext = CoreDataManager.managedObjectContext
        let fetchRequest:NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: entityName)

        do {
            let results = try managedContext.fetch(fetchRequest)
            if let resultsArray = results as? [NSManagedObject] {
                return resultsArray.count == 0
            }

        } catch {
            print("Error: \(error.localizedDescription)")
            return true
        }
        return true
    }

    func deletePhotosFromCoreData (_ coreDataArray: [NSManagedObject]) {
        let managedContext = CoreDataManager.managedObjectContext

        for object in coreDataArray {
            managedContext.delete(object)
        }
    }

    func savePhotos(_ photos: [Photo]) -> [NSManagedObject] {
        var photosCoreDataArray = [NSManagedObject]()
        let managedContext = CoreDataManager.managedObjectContext

        if let entity = NSEntityDescription.entity(forEntityName: "PhotoEntity", in:managedContext) {
            for photoInfo in photos {
                let photo = NSManagedObject(entity: entity, insertInto: managedContext)
                photo.setValue(photoInfo.id, forKey: "id")
                photo.setValue(photoInfo.owner, forKey: "owner")
                photo.setValue(photoInfo.secret, forKey: "secret")
                photo.setValue(photoInfo.server, forKey: "server")
                photo.setValue(photoInfo.farm, forKey: "farm")
                photo.setValue(photoInfo.title, forKey: "title")
                photo.setValue(photoInfo.isPublic, forKey: "isPublic")
                photo.setValue(photoInfo.isFriend, forKey: "isFriend")
                photo.setValue(photoInfo.isFamily, forKey: "isFamily")
                photo.setValue(photoInfo.visualized, forKey: "visualized")
                do {
                    try managedContext.save()
                    photosCoreDataArray.append(photo)
                } catch {
                    print("Could not save: \(error.localizedDescription)")
                }
            }
        }

        return photosCoreDataArray
    }

    func createJsonFrom(_ photoCoreData: NSManagedObject) -> JSON? {
        guard let id = photoCoreData.value(forKey: "id") as? String,
            let owner = photoCoreData.value(forKey: "owner") as? String,
            let secret = photoCoreData.value(forKey: "secret") as? String,
            let server = photoCoreData.value(forKey: "server") as? String,
            let farm = photoCoreData.value(forKey: "farm") as? Int,
            let title = photoCoreData.value(forKey: "title") as? String,
            let isPublic = photoCoreData.value(forKey: "isPublic") as? Int,
            let isFriend = photoCoreData.value(forKey: "isFriend") as? Int,
            let isFamily = photoCoreData.value(forKey: "isFamily") as? Int,
            let visualized = photoCoreData.value(forKey: "visualized") as? Bool else {
                return nil
        }
        let json: JSON = ["id": id,
                          "owner": owner,
                          "secret": secret,
                          "server": server,
                          "farm": farm,
                          "title": title,
                          "isPublic": isPublic,
                          "isFriend": isFriend,
                          "isFamily": isFamily,
                          "visualized": visualized]
        return json
    }

    func updatePhoto(_ id: String?, visualized: Bool) {
        let managedContext = CoreDataManager.managedObjectContext

        let fetchRequest:NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: "PhotoEntity")

        if let id = id {
            fetchRequest.predicate = NSPredicate(format: "id == %@", id)
        }

        do {
            let results = try managedContext.fetch(fetchRequest)
            guard let photosCoreData = results as? [NSManagedObject] else {
                return
            }
            for photoCoreData in photosCoreData {
                photoCoreData.setValue(visualized, forKey: "visualized")
            }
            try managedContext.save()
        } catch {
            print("Could not fetch: \(error.localizedDescription)")
        }

    }

    func fetchPhotosFromCoreData(id: String? = nil) -> ([Photo]?, [NSManagedObject]?) {
        var photos = [Photo]()
        let managedContext = CoreDataManager.managedObjectContext

        let fetchRequest:NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: "PhotoEntity")

        if let id = id {
            fetchRequest.predicate = NSPredicate(format: "id == %@", id)
        }

        do {
            let results = try managedContext.fetch(fetchRequest)
            guard let photosCoreData = results as? [NSManagedObject] else {
                return (nil, nil)
            }
            for photoCoreData in photosCoreData {
                if let json = createJsonFrom(photoCoreData) {
                    let photo = Photo(withJson: json)
                    photos.append(photo)
                }
            }
            return (photos, photosCoreData)
        } catch {
            print("Could not fetch: \(error.localizedDescription)")
        }
        return (nil, nil)
    }

    static var applicationDocumentsDirectory: URL = {

        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return urls[urls.count-1]
    }()

    static var managedObjectModel: NSManagedObjectModel = {
        let modelURL = Bundle(for: CoreDataManager.self).url(forResource: "Flicker_Photo_Viewer", withExtension: "momd")!
        return NSManagedObjectModel(contentsOf: modelURL)!
    }()

    static var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: managedObjectModel)
        let url = applicationDocumentsDirectory.appendingPathComponent("Flicker_Photo_Viewer.sqlite")
        var failureReason = "There was an error creating or loading the application's saved data."
        let options = [NSMigratePersistentStoresAutomaticallyOption: NSNumber(value: true as Bool), NSInferMappingModelAutomaticallyOption: NSNumber(value: true as Bool)]
        do {
            try coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: options)
        } catch {
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data" as AnyObject
            dict[NSLocalizedFailureReasonErrorKey] = failureReason as AnyObject

            dict[NSUnderlyingErrorKey] = error as NSError
            let wrappedError = NSError(domain: "ERROR", code: 9999, userInfo: dict)
            NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
            abort()
        }

        return coordinator
    }()

    static var managedObjectContext: NSManagedObjectContext = {
        let coordinator = persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()

    // MARK: - Core Data Saving support

    static func saveContext () {
        if managedObjectContext.hasChanges {
            do {
                try managedObjectContext.save()
            } catch (let error) {
                print("Unresolved error \(error.localizedDescription)")
                abort()
            }
        }
    }
}
