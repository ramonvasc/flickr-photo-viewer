//
//  DetailsViewController.swift
//  Flicker Photo Viewer
//
//  Created by Antônio Ramon Vasconcelos de Freitas on 07/09/17.
//  Copyright © 2017 Ramon Vasconcelos. All rights reserved.
//

import UIKit

class DetailsViewController: UIViewController {

    @IBOutlet weak var picture: UIImageView!
    var photo: Photo?

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Details"
        guard let photo = photo, let url = URL.flickerPhotoURLFrom(photo: photo, size: .medium) else {
            return
        }
        picture.kf.setImage(with: url)

    }

}
