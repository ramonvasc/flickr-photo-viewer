//
//  ServerManager.swift
//  Flicker Photo Viewer
//
//  Created by Ramon Vasconcelos on 07/09/2017.
//  Copyright © 2017 Ramon Vasconcelos. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

struct DiscoveryAPI {

    private static let backgroundQueue = DispatchQueue.global(qos: .background)
    typealias PhotosCompletion = ([Photo]) -> Void

    static func getRecentPhotos(completion: PhotosCompletion?) {
        backgroundQueue.async {
            let application: UIApplication = UIApplication.shared
            if !application.connectedToNetwork() {
                requestDataLocally(completion)
            } else {
                requestDataRemotely(completion)
            }
        }
    }

    static func requestDataLocally(_ completion: PhotosCompletion?) {
        guard let photos = CoreDataManager().fetchPhotosFromCoreData().0 else {
            completion?([])
            return
        }
        completion?(photos)
    }

    static func requestDataRemotely(_ completion: PhotosCompletion?) {
        guard let url = URL(string: baseUrl) else {
            completion?([])
            return
        }

        let parameters = ["method": FlickerMethods.getRecentPhotos,
                          "api_key": apiKey,
                          "format": "json",
                          "nojsoncallback": "1"]
        Alamofire.request(url, parameters: parameters).responseJSON { (response) in
            switch response.result {
            case .success(let value):
                deleteOldCoreDataEntries()
                let responseJSON = JSON(value)
                guard let photosJson = responseJSON["photos"]["photo"].array else {
                    return
                }
                var photos = [Photo]()
                for json in photosJson {
                    let photo = Photo(withJson: json)
                    photos.append(photo)
                }
                _ = CoreDataManager().savePhotos(photos)
                completion?(photos)
            case .failure(let error):
                print("error: \(error.localizedDescription)")
                completion?([])
            }
        }
    }

    static func deleteOldCoreDataEntries() {
        if !CoreDataManager().entityIsEmpty("PhotoEntity") {
            guard let objectsToDelete = CoreDataManager().fetchPhotosFromCoreData().1 else {
                return
            }
            CoreDataManager().deletePhotosFromCoreData(objectsToDelete)
        }
    }
    
}
