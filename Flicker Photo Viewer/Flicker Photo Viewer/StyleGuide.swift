//
//  StyleGuide.swift
//  Flicker Photo Viewer
//
//  Created by Antônio Ramon Vasconcelos de Freitas on 07/09/17.
//  Copyright © 2017 Ramon Vasconcelos. All rights reserved.
//

import UIKit

extension UIColor {

    class var lightBlue: UIColor {
        return UIColor(colorLiteralRed: 0 / 255, green: 128 / 255, blue: 1, alpha: 1)
    }

}

extension UIFont {

    class func photoCellTextFont() -> UIFont? {
        return UIFont.systemFont(ofSize: 17)
    }
}
