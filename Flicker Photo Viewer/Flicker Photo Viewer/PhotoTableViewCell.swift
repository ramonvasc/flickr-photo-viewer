//
//  PhotoTableViewCell.swift
//  Flicker Photo Viewer
//
//  Created by Antônio Ramon Vasconcelos de Freitas on 07/09/17.
//  Copyright © 2017 Ramon Vasconcelos. All rights reserved.
//

import UIKit

class PhotoTableViewCell: UITableViewCell {

    @IBOutlet weak var picture: UIImageView!
    @IBOutlet weak var title: UILabel!

}
