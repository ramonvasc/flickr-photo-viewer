//
//  InitialViewController.swift
//  Flicker Photo Viewer
//
//  Created by Ramon Vasconcelos on 07/09/2017.
//  Copyright © 2017 Ramon Vasconcelos. All rights reserved.
//

import UIKit
import Kingfisher
import SVPullToRefresh_Bell

class InitialViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var photos = [Photo]()

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Flicker Photo Viewer"
        setupCells()
        setupPullToRefresh()
        requestPhotos()
    }

    override func viewWillAppear(_ animated: Bool) {
        tableView.reloadData()
    }

    func setupPullToRefresh() {
        tableView.addPullToRefresh {
            weak var weakSelf = self
            weakSelf?.requestPhotos()
        }
    }

    func requestPhotos() {
        DiscoveryAPI.getRecentPhotos { (photos) in
            DispatchQueue.main.async {
                self.photos = photos
                self.tableView.reloadData()
            }
        }
    }

    func setupCells() {
        let photoNib = UINib(nibName: "PhotoTableViewCell", bundle: nil)
        tableView.register(photoNib, forCellReuseIdentifier: "PhotoTableViewCell")
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "largeImageSegue", let destination = segue.destination as? DetailsViewController, let photo = sender as? Photo {
            destination.photo = photo
        }
    }

}

extension InitialViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return photos.count
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PhotoTableViewCell", for: indexPath) as! PhotoTableViewCell
        cell.title.text = photos[indexPath.row].title
        if let url = URL.flickerPhotoURLFrom(photo: photos[indexPath.row], size: .small) {
            cell.picture.kf.setImage(with: url)
        }

        cell.selectionStyle = .none
        cell.title.font = UIFont.photoCellTextFont()

        if let visualized = photos[indexPath.row].visualized, visualized {
            cell.backgroundColor = UIColor.white
        } else {
            cell.backgroundColor = UIColor.lightBlue
        }

        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        photos[indexPath.row].visualized = true
        CoreDataManager().updatePhoto(photos[indexPath.row].id, visualized: true)
        performSegue(withIdentifier: "largeImageSegue", sender: photos[indexPath.row])
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }

}
