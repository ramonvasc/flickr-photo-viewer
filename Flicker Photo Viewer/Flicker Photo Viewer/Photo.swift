//
//  Photo.swift
//  Flicker Photo Viewer
//
//  Created by Ramon Vasconcelos on 07/09/2017.
//  Copyright © 2017 Ramon Vasconcelos. All rights reserved.
//

import UIKit
import SwiftyJSON

class Photo {
    var id: String?
    var owner: String?
    var secret: String?
    var server: String?
    var farm: Int?
    var title: String?
    var isPublic: Int?
    var isFriend: Int?
    var isFamily: Int?
    var visualized: Bool?

    init(withJson json: JSON) {
        id = json["id"].string
        owner = json["owner"].string
        secret = json["secret"].string
        server = json["server"].string
        farm = json["farm"].int
        title = json["title"].string
        isPublic = json["ispublic"].int
        isFriend = json["isfriend"].int
        isFamily = json["isfamily"].int
        visualized = json["visualized"].bool ?? false
    }

}

extension URL {
    static func flickerPhotoURLFrom(photo: Photo, size: FlickerPhotoSize) -> URL? {
        guard let farm = photo.farm, let server = photo.server, let photoID = photo.id, let secret = photo.secret else {
            return nil
        }
        let urlString = "https://farm\(farm).staticflickr.com/\(server)/\(photoID)_\(secret)_\(size.rawValue).jpg"
        return URL(string: urlString)
    }
}
